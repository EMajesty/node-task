# Node Task

This project contains node hello world

## Installation steps

1. Copy the repository
2. Install dependencies
3. Start the app

## Terminal commands

```sh
git clone https://gitlab.com/EMajesty/node-task
cd ./node-task
npm i
```

Start cmd: `npm run start`

## App should work

![kuva](rutto.png)